﻿namespace Common
{
    public class Enums
    {
        public enum Status
        {
            None = 0,
            Active = 1
        }

        public enum Permission
        {
            Base,
            Common,
            StudentManager
        }
    }
}
