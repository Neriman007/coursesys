﻿using BLL.DTOs.BaseDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Common.Constants.Enums;

namespace BLL.DTOs.UserDTOs
{
    public class UserPermissionDTO : BaseDTO
    {
        public long UserRoleId { get; set; }
        public Permission Permission { get; set; }
        public string Description { get; set; }
        public UserRoleDTO UserRole { get; set; }
    }
}