﻿using BLL.DTOs.BaseDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.DTOs.UserDTOs
{
    public class UserRoleDTO : BaseDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
