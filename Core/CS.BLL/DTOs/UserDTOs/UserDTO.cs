﻿using BLL.DTOs.BaseDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Common.Constants.Enums;

namespace BLL.DTOs.UserDTOs
{
    public class UserDTO : BaseDTO
    {
        public long UserRoleId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public UserRoleDTO UserRole { get; set; }
    }
}
