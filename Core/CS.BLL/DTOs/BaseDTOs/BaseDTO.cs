﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Common.Constants.Enums;

namespace BLL.DTOs.BaseDTOs
{
    public abstract class BaseDTO
    {
        public long Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public Status Status { get; set; }
        public long CreatedUserId { get; set; }
        public long ModifiedUserId { get; set; }
    }
}
