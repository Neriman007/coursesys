﻿using System.Data.Entity;

namespace DAL.DataContext
{
    public class MainDataContext : DbContext
    {
        public MainDataContext() : base("name=")
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<UserPermission> UserPermissions { get; set; }
    }
}