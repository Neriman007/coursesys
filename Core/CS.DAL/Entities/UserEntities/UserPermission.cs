﻿using System.ComponentModel.DataAnnotations.Schema;
using static Common.Enums;

namespace DAL
{
    public class UserPermission : BaseEntity
    {
        public long UserRoleId { get; set; }
        public Permission Permission { get; set; }
        public string Description { get; set; }

        [ForeignKey(nameof(UserRoleId))]
        public virtual UserRole UserRole { get; set; }
    }
}