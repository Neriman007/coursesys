﻿using System;
using System.ComponentModel.DataAnnotations;
using static Common.Enums;

namespace DAL
{
    public abstract class BaseEntity
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public Status Status { get; set; }
        public long CreatedUserId { get; set; }
        public long ModifiedUserId { get; set; }

    }
}